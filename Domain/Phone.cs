﻿namespace Domain;

public class Phone
{
    public int CountryCode { get; set; }
    public string Number { get; set; }
}
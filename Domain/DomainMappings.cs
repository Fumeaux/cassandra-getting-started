﻿using Cassandra.Mapping;

namespace Domain;

public class DomainMappings : Mappings
{
    public static void LoadMappings()
    {
        MappingConfiguration.Global.Define(
            new Map<ShoppingCart>()
                .TableName("shopping_cart")
                .PartitionKey(c => c.Id)
                .Column(c => c.Id, cm => cm.WithName("shoppingcartid"))
                .Column(c => c.ItemCount, cm => cm.WithName("item_count"))
                .Column(c => c.LastUpdateTimestamp, cm => cm.WithName("last_update_timestamp")),
            new Map<Phone>()
                .Column(c => c.CountryCode, cm => cm.WithName("country_code"))
                .Column(c => c.Number, cm => cm.WithName("number"))
        );
    }
}
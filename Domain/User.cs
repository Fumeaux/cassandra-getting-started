﻿using System.ComponentModel.DataAnnotations;

namespace Domain;

public class User
{
    public string UserId { get; set; }
    
    public string Name { get; set; }
    
    [Timestamp]
    public DateTime Last_Update_Timestamp { get; set; }

    public Dictionary<string, Phone> Phones { get; set; }
    
    public List<string> Emails { get; set; }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Domain;

public class ShoppingCart
{
    public string Id { get; set; }
    
    public int ItemCount { get; set; }
    
    [Timestamp]
    public DateTime LastUpdateTimestamp { get; set; }
}
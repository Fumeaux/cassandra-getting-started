using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using Domain;
using NUnit.Framework;

namespace Client;

public class CassandraQueries
{
    private Cluster _cluster;
    private ISession _session;
    private Mapper _mapper;

    [SetUp]
    public void Setup()
    {
        DomainMappings.LoadMappings();

        var hostName = Dns.GetHostName();
        var localAdd = Dns.GetHostEntry(hostName).AddressList.First();

        _cluster = Cluster.Builder()
            .AddContactPoints(new IPEndPoint(localAdd, 9042), new IPEndPoint(localAdd, 19042),
                new IPEndPoint(localAdd, 29042))
            .WithDefaultKeyspace("store")
            .Build();

        _session = _cluster.ConnectAndCreateDefaultKeyspaceIfNotExists(new Dictionary<string, string>
            { { "class", "SimpleStrategy" }, { "replication_factor", "2" } });
        try
        {
            _session.UserDefinedTypes.Define(UdtMap.For<Phone>());
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        _mapper = new Mapper(_session);
    }

    [Test, Explicit]
    public void DeleteKeyspace()
    {
        _session.Execute("DROP KEYSPACE store;");
    }

    [Test, Explicit]
    public void CreateTables()
    {
        _session.Execute(@"CREATE TYPE IF NOT EXISTS phone (
    country_code int,
    number text,
);");

        _session.Execute(@"CREATE TABLE IF NOT EXISTS store.user (
    userid text PRIMARY KEY,
    name TEXT,
    last_update_timestamp TIMESTAMP,
    phones map<text, frozen<phone>>,
    emails list<text>,
);");
        _session.Execute(@"CREATE TABLE IF NOT EXISTS store.shopping_cart (
    shoppingcartid text PRIMARY KEY,
    item_count INT,
    last_update_timestamp TIMESTAMP
);");
    }

    [Test]
    public void InsertCql()
    {
        _session.Execute(@"INSERT INTO store.user
        (userid, name, last_update_timestamp)
        VALUES ('1', 'User1', toTimeStamp(now()));");
    }

    [Test]
    public void InsertData()
    {
        var newUser = new User
        {
            UserId = "2", Name = "User2", Last_Update_Timestamp = DateTime.Now,
            Emails = new List<string> { "email@email", "email2@email" }
        };

        var newPhone = new Phone { CountryCode = 12, Number = "1234-1234" };
        newUser.Phones = new Dictionary<string, Phone>
        {
            { "Mobile", newPhone }
        };

        _mapper.Insert(newUser);
    }

    [Test]
    public void GetAllUsers()
    {
        var result = _mapper.Fetch<User>();

        foreach (var user in result)
        {
            Print(user);
        }
    }
    
    [Test]
    public void GetOneUserCQL()
    {
        var user1 = _mapper.First<User>("FROM store.user WHERE userid = ?", "1");
        Print(user1);
    }

    [Test]
    public void GetOneUserLinq()
    {
        var users = new Table<User>(_session);

        var user2 = users.First(u => u.UserId == "2").Execute();

        Print(user2);
    }

    [Test]
    public async Task InsertShoppingCart()
    {
        var newShoppingCart = new ShoppingCart
        {
            Id = "1",
            ItemCount = 2,
            LastUpdateTimestamp = DateTime.Now
        };

        await _mapper.InsertAsync(newShoppingCart);
    }

    [Test]
    public async Task GetAllShoppingCarts()
    {
        foreach (var shoppingCart in await _mapper.FetchAsync<ShoppingCart>())
        {
            Print(shoppingCart);
        }
    }

    #region Print

    private void Print(User user)
    {
        Console.WriteLine($"Id: '{user.UserId}', Name: '{user.Name}', TimeStamp: '{user.Last_Update_Timestamp}', Emails: '{string.Join(", ", user.Emails)}'");
        foreach (var (name, phone) in user.Phones)
        {
            Print(name, phone);
        }
    }

    private void Print(string name, Phone phone)
    {
        Console.WriteLine(
            $"- {name}: {nameof(Phone.CountryCode)}: '{phone.CountryCode}', {nameof(Phone.Number)}: '{phone.Number}'");
    }

    private void Print(ShoppingCart shoppingCart)
    {
        Console.WriteLine(
            $"{nameof(ShoppingCart.Id)}: '{shoppingCart.Id}', {nameof(ShoppingCart.ItemCount)}: '{shoppingCart.ItemCount}', {nameof(ShoppingCart.LastUpdateTimestamp)}: '{shoppingCart.LastUpdateTimestamp}'");
    }

    #endregion
}